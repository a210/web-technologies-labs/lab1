package tp1;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class ShowSessionServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();

        int visitCount = 0;
        if (session.getAttribute("visitCount") != null) {
            visitCount = (int) session.getAttribute("visitCount");
        }
        session.setAttribute("visitCount", visitCount + 1);


        ServletOutputStream out = response.getOutputStream();
        out.println("<HTML>\n");
        out.println("<HEAD>\n");
        out.println("<TITLE>Show Session</TITLE>\n");
        out.println("</HEAD>\n");
        out.println("<BODY>\n");

        if (visitCount == 0) {
            out.println("<H1>Welcome on my site</H1>\n");
        } else {
            out.println("<H1>Welcome back</H1>\n");
        }

        out.println("<p>count: " + visitCount+ "</p>");
        out.println("</BODY>\n");
        out.println("</HTML>");
    }
}
