package tp1;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class FormServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");

        resp.setContentType("text/html");
        ServletOutputStream out = resp.getOutputStream();
        out.println("<HTML>\n");
        out.println("<HEAD>\n");
        out.println("<TITLE>Handling a POST request</TITLE>\n");
        out.println("</HEAD>\n");
        out.println("<BODY>\n");
        out.println("<H1>Handling a POST request</H1>\n");
        out.println("<form method=post>\n " +
                "<label for=\"firstname\">firstname:</label>\n" +
                "    <input id=\"firstname\" type=\"text\" placeholder=\"firstname\" name=\"firstname\">\n" +
                "    <label for=\"lastname\">lastname:</label>\n" +
                "    <input id=\"lastname\" type=\"text\" placeholder=\"lastname\" name=\"lastname\">\n" +
                "    <label for=\"birthday\">Birthday:</label>\n" +
                "    <input type=\"date\" id=\"birthday\" name=\"birthday\">\n" +
                "   <input type=\"radio\" id=\"sex1\"\n" +
                "       name=\"sex\" value=\"M\">\n" +
                "<label for=\"sex1\">Male</label>\n" +
                "\n" +
                "<input type=\"radio\" id=\"sex2\"\n" +
                "       name=\"sex\" value=\"F\">\n" +
                "<label for=\"sex2\">Female</label>\n" +
                "\n" +
                "<input type=\"radio\" id=\"sex3\"\n" +
                "       name=\"sex\" value=\"U\">\n" +
                "<label for=\"sex3\">Undefined</label>\n" +
                "    <input type=\"submit\">\n" +
                "<a href=\"/tp1/ReadCookieServlet\">read cookies</a>" +
                "   </form>");
        out.println("</BODY>\n");
        out.println("</HTML>");

    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String firstname = req.getParameter("firstname");
        String lastname = req.getParameter("lastname");
        String birthday = req.getParameter("birthday");
        String sex = req.getParameter("sex");

        Cookie firstnameCookie = new Cookie("firstname", firstname);
        Cookie lastnameCookie = new Cookie("lastname", lastname);
        Cookie birthdayCookie = new Cookie("birthday", birthday);
        Cookie sexCookie = new Cookie("sex", sex);

        resp.addCookie(firstnameCookie);
        resp.addCookie(lastnameCookie);
        resp.addCookie(birthdayCookie);
        resp.addCookie(sexCookie);

        ServletOutputStream out = resp.getOutputStream();
        out.println("<HTML>\n");
        out.println("<HEAD>\n");
        out.println("<TITLE>Handling a POST request</TITLE>\n");
        out.println("</HEAD>\n");
        out.println("<BODY>\n");
        out.println("<H1>Handling a POST request</H1>\n");
        out.println("<table>\n" +
                "    <tr>\n" +
                "        <th>key</th>\n" +
                "        <th>value</th>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "        <td>Firstname</td>\n" +
                "        <td>" + firstname + "</td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "        <td>Lastname</td>\n" +
                "        <td>" + lastname + "</td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "        <td>Birthday</td>\n" +
                "        <td>" + birthday + "</td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "        <td>Sex</td>\n" +
                "        <td>" + sex + "</td>\n" +
                "    </tr>\n" +
                "</table>");
        out.println("</BODY>\n");
        out.println("</HTML>");

    }
}
