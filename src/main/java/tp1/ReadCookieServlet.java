package tp1;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ReadCookieServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Cookie[] cookies = request.getCookies();

        response.setCharacterEncoding("utf-8");
        response.setContentType("text/html");

        ServletOutputStream out = response.getOutputStream();
        out.println("<HTML>\n");
        out.println("<HEAD>\n");
        out.println("<TITLE>Handling a POST request</TITLE>\n");
        out.println("<BODY>\n");
        out.println("<H1>Reading Cookies</H1>\n");
        for (int i = 0; i < cookies.length; i++) {
            out.println("<p>Name: " + cookies[i].getName() + " & Value:" + cookies[i].getValue() + "+</p>\n");

        }
        out.println("</BODY>\n");
        out.println("</HTML>");
    }
}
