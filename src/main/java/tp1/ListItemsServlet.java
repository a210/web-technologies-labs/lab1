package tp1;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

public class ListItemsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        ArrayList<String> listItems = new ArrayList<>();

        HttpSession session = request.getSession();
        if (session.getAttribute("listItems") != null) {
            listItems = (ArrayList<String>) session.getAttribute("listItems");
        }

        listItems.add(request.getParameter("newItem"));

        session.setAttribute("listItems", listItems);

        ServletOutputStream out = response.getOutputStream();
        out.println("<HTML>\n");
        out.println("<HEAD>\n");
        out.println("<TITLE>Handling a POST request</TITLE>\n");
        out.println("</HEAD>\n");
        out.println("<BODY>\n");
        out.println("<H1>Handling a POST request</H1>\n");
        out.println("<ul>");

        for (String item : listItems) {
            out.println("<ul>=>" + item + "</ul>");
        }
        out.println("</ul>");
        out.println("<a href=\"items.html\">go back to form</a>");
        out.println("</BODY>\n");
        out.println("</HTML>");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
