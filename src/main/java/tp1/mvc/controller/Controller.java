package tp1.mvc.controller;

import tp1.mvc.model.Person;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Controller extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Person person = new Person(
                request.getParameter("firstname"),
                request.getParameter("lastname"),
                request.getParameter("birthday"),
                request.getParameter("sex"));

        String page = "/mvc/young.jsp";
        // verifying the date
        String[] dateSplit = request.getParameter("birthday").split("-");
        int date = getDate(dateSplit);

        if (date < 1900) {
            page = "/mvc/old.jsp";
        }

        request.setAttribute("person", person);

        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
        dispatcher.include(request, response);
    }

    private static int getDate(String[] dateArray) {
        return Integer.parseInt(dateArray[0]);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
