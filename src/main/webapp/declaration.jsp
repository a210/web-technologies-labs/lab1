<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.text.DateFormat" %><%--
  Created by IntelliJ IDEA.
  User: andrewpouret
  Date: 2020-03-12
  Time: 22:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Random List</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <%! private int numEntries = 200;

        private int randomInt(int range) {
            return (1 + ((int) (Math.random() * range)));
        } %>
    <h1>A random list from 1 to 100:</h1>
    <ul>
        <% for (int i = 0; i < numEntries; i++) { %>
        <li><%= randomInt(100) %>
        </li>
        <% } %>
    </ul>
</div>
</body>
</html>
