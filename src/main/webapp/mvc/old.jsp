<%--
  Created by IntelliJ IDEA.
  User: andrewpouret
  Date: 2020-03-12
  Time: 22:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <title>Person</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
</head>
<body>

<div class="container">
    <h1>Bean Person Old</h1>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">value</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th scope="row">Firstname</th>
            <td>
                ${person.firstname}
            </td>
        </tr>
        <tr>
            <th scope="row">Lastname</th>
            <td>
                ${person.lastname}
            </td>
        </tr>
        <tr>
            <th scope="row">Birthday</th>
            <td>
                ${person.birthday}
            </td>
        </tr>
        <tr>
            <th scope="row">Sex</th>
            <td>
                ${person.sex}
            </td>
        </tr>
        </tbody>
    </table>
    <a href="../mvc/index.html" class="btn btn-primary stretched-link">Create Person</a>
</div>

</body>
</html>
